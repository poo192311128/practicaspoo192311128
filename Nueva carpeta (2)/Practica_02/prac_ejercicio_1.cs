﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Suma(10, 4));
            Console.WriteLine(Suma(15, 35, 23));
            Console.WriteLine(Suma(10, 12, 12, 18));
            Console.ReadKey();
        }
        public static int Suma(int a, int b)
        {
            return a + b;
        }
        public static int Suma(int a, int b, int c)
        {
            return a + b + c;
        }
        public static int Suma(int a, int b, int c, int d)
        {
            return a + b + c + d;
        }

    }
}