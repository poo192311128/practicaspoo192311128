﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Burbuja // CREAMOS UNA CLASE LLAMADO BURBUJA, POR EL METODO.
{
    private int[] lista; //CREAMOS UN ARRAY LLAMADO LISTA, ENCAPSULADO

    public void LlenadoLista() //METODO PARA LLENAR LA LISTA
    {
        Console.WriteLine("Listas Con metodo burbuja");
        Console.Write("Cuantos elementos tendra la lista: ");
        string linea; // DECLARAMOS UNA VARIABLE DEL TIPO STRING, LLAMDADA LINEA.
        linea = Console.ReadLine(); //ESTA LINEA TENDRA UN VALOR, DEPENDIENOD DE LO QUE SE LE PONGA EN CONSOLA
        int cant; // DECLARAMOS UNA VARIABLE LLAMADA CANT, PERO AHORA DEL TIPO INT
        cant = int.Parse(linea); //CANT SERA IGUAL ALO QUE ESTA ALMACENADO EN LINEA,PERO COMO LINEA ES STRING, LO CONVERTIMOS A ENTERO
        lista = new int[cant]; //AQUI MANDAMOS LLAMAR AL ARRAY LISTA QUE DECLARAMOS AL PRINCIPIO, Y LE DECIMOS QUE ES IGUAL A LO QUE ESTA ALMACENADO EN "CANT"
        for (int f = 0; f < lista.Length; f++) // PARA F=0, MIENTRAS F SEA MENOR ALA CANTIDAD DE NUMEROS QUE TENGA LISTA, AUMENTA EN F EN 1 POR CADA VUELTA DE BUCLE
        {
            Console.Write("Ingrese elemento " + (f + 1) + ": "); // AQUI LE DECIMOS QUE INGRESE EL ELEMENTO 1, PERO COMO F=0 Y SE TOMA EL INDICE EMPEZANDO EN 0, PUES DECIMOS QUE F+1 PARA NO CONFUNDIR AL USUARIO
            linea = Console.ReadLine(); // AQUI DECLARAMOS QUE LINEA SERA ALO QUE INTRODUZCAMOS EN CONSOLA.
            lista[f] = int.Parse(linea); // AQUI DECIMOS QUE LISTA[ DESDE SU INDICE 0, POR QUE F=0], EL VALOR EN F VALDRA LO QUE INTRODUCIMOS EN LINEA , EN LA INSTRUCCION PASADA
        }
    }
    //BIEN YA TENEMOS UNA ARREGLO CREADO Y CON SUS RESPECTIVOS VALORES
    //AHORA SOLO FALTARIA ACOMODARLOS.

    public void MetodoBurbuja() //ESTE METODO NOS PERMITIRA ACOMODAR EN LINEA 
    {
        int t; //DECLARAMOS UNA VARIABLE ENTERA EN I
        for (int a = 1; a < lista.Length; a++) //DECLARAMOS UN CONTADOR "A" EN 1, Y DECIMOS QUE MIENTRAS LO QUE VALGA SEA MENOR AL NUMERO ALMACENADO EN LISTA, INCREMENTAR "A" EN 1 POR CADA VUELTA DE BUCLE
                                               //AQUI DECLARAMOS UN FOR ANIDADO ASI QUE LOS FOR ANIDADOS, SE DESARROLLAN DE ADENTRO A AFUERA, ASI QUE PRIMERO SE EJECUTARA EL ULTIMO FOR, HASTA LLEGAR AL PRIMERO.

            for (int b = lista.Length - 1; b >= a; b--) // PARA UN CONTADOR B QUE ES IGUAL, ALOS VALORES TOTALES QUE TENEMOS ALMACENADOS EN LISTA.. RESTARLE 1
                                                        // AHORA , MIENTRAS B SEA MAYOR O IGUAL A "A", 
                                                        //DECREMENTA B EN 1, EJEMPLO: SI B ES 2, POR CADA VUELTA DE BUCLE SE LE RESTA 1 A "B".
            {
                if (lista[b - 1] > lista[b]) // SI EL INDICE REDUCIDO EN 1 DE LISTA, ES MAYOR A.. INDICE QUE ESTA ALMACENADA EN LISTA:
                {
                    t = lista[b - 1]; //AQUI CREAMOS UNA VARIABLE QUE ES IGUAL ALO QUE ESTE ALMACENADO EN EL INDICE DE LISTA MENOS 1.
                    lista[b - 1] = lista[b]; //AHORA LO QUE ESTE ALMACENADO EN EL INDICE DE LISTA MENOS 1, SERA IGUAL A LO QUE ESTE ALMACENADO EN EL INDICE DE LISTA.
                    lista[b] = t; //ENTONCES LO QUE ESTA ALMACENADO EN EL INDICE DE B, SERIA IGUAL A T.
                }
            }
    }

    public void Imprimir() //ESTE METODO NOS PERMITIRA RECORRER LA LISTA CREADA EN EL ARRAY
    {
        Console.WriteLine("Lista ordenada en forma ascendente");
        for (int f = 0; f < lista.Length; f++) // AQUI SOLO RECORREMOS TODA LA LISTA 
        {
            Console.Write(lista[f] + "  "); //ENTONCES COMO F=0 , IMPRIME LO QUE ESTA EN EL INDICE 0, Y CON UN ESPACIO EN BLANCO
                                            // COMO F, CAMBIA DE VALOR POR CADA VUELTA DE BUCLE , PUES NOS IRA MOSTRANDO DISTINTOS RESULTADOS
        }
        Console.ReadKey(); // ESTE METODO DE LA CLASE CONSOLA NOS PERMITE TERMINAR UN PROCESO AL PRESIONAR UNA TECLA, OBVIAMENTE ESTE METODO SE EJECUTA DESPUES DE EJECUTAR EL BUCLE
                           //ASI QUE LO QUE ESTE FUERA DE ESTE METODO, SE CERRARA O NO SE LOGRARA EJECUTAR OTRAS LINEAS DE CODIGO.
    }


}





class Program
{
    static void Main(string[] args)
    {
        // AQUI MANDAMOS LLAMAR ALA CLASE , CON UN NOMBRE CUALQUIERA. PARA PODER EJECUTAR LOS METODOS, YA QUE EL PRINCIPAL LO DEJE EN PRIVADO
        Burbuja pv = new Burbuja();
        
        pv.LlenadoLista();
        pv.MetodoBurbuja();
        pv.Imprimir();
    }
}

