﻿using System;

namespace Perimetro
{
    namespace perimetro
    {
        class Program
        {
            static void Main(string[] args)
            {
                //Variables
                double Radio2, Perimetro1;
                const double PI = 3.1415;
                //Entrada
                System.Console.Write(" Valor radio es: ");
                Radio2 = double.Parse(Console.ReadLine());
                //Desarrollo
                Perimetro1 = 2 * PI * Radio2;
                //Dts Salida
                System.Console.Write("El Perimetro es igual a: " + Perimetro1);
                System.Console.ReadKey();
            }
        }
    }
}