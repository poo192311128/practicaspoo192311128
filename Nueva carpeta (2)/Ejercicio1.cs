﻿using System;

namespace Circunferencia
{
    class Program
    {
        static void Main(string[] arg)

        {
            //variables
            double radio, area;
            const double PI = 3.1415;
            //Entrada
            System.Console.Write(" Valor del radio es: ");
            radio = double.Parse(Console.ReadLine());
            //Desarrollo
            area = PI * Math.Pow(radio, 2);
            //Dts Salida
            System.Console.Write("El area es igual a: " + area);
            System.Console.ReadKey();
        }
    }
}

